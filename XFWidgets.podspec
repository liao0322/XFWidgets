#
# Be sure to run `pod lib lint XFWidgets.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'XFWidgets'
  s.version          = '0.6.4'
  s.summary          = 'XFWidgets.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/liao0322/XFWidgets'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'liao0322' => 'liao0322@hotmail.com' }
  s.source           = { :git => 'https://gitlab.com/liao0322/XFWidgets.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

# s.source_files = 'XFWidgets/Classes/**/*'

    s.subspec 'XFButton' do |btn|
      btn.source_files = 'XFWidgets/Classes/XFButton/**/*'
    end
    s.subspec 'XFProgressHUD' do |hud|
      hud.source_files = 'XFWidgets/Classes/XFProgressHUD/**/*.{h,m}'
      hud.resource_bundles = {
          'XFWidgets' => ['XFWidgets/Assets/*.png']
      }
      hud.dependency 'MBProgressHUD'
      hud.dependency 'FLCategories'
    end
    s.subspec 'XFTextField' do |tf|
        tf.source_files = 'XFWidgets/Classes/XFTextField/**/*'
        tf.dependency 'FLCategories'
    end
    s.subspec 'XFTextCarouselView' do |tcv|
        tcv.source_files = 'XFWidgets/Classes/XFTextCarouselView/**/*'
    end


  
  # s.resource_bundles = {
  #   'XFWidgets' => ['XFWidgets/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
# s.dependency 'MBProgressHUD'
end
