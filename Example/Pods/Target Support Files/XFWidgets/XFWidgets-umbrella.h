#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "XFNoHighlightButton.h"
#import "XFProgressHUD.h"
#import "XFTextCarouselView.h"
#import "XFTextField.h"

FOUNDATION_EXPORT double XFWidgetsVersionNumber;
FOUNDATION_EXPORT const unsigned char XFWidgetsVersionString[];

