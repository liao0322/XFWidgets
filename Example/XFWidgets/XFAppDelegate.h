//
//  XFAppDelegate.h
//  XFWidgets
//
//  Created by liao0322 on 03/18/2018.
//  Copyright (c) 2018 liao0322. All rights reserved.
//

@import UIKit;

@interface XFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
