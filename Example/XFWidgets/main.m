//
//  main.m
//  XFWidgets
//
//  Created by liao0322 on 03/18/2018.
//  Copyright (c) 2018 liao0322. All rights reserved.
//

@import UIKit;
#import "XFAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([XFAppDelegate class]));
    }
}
