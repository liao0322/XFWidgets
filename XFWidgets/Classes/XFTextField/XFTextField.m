//
//  XFTextField.m
//  FengLei
//
//  Created by liaoxf on 2018/1/12.
//  Copyright © 2018年 com.mlj.FengLei. All rights reserved.
//

#import "XFTextField.h"
#import "UIView+Extension.h"

@implementation XFTextField

- (CGRect)leftViewRectForBounds:(CGRect)bounds {
    [self subViewOfClassName:@""];
    CGRect iconRect = [super leftViewRectForBounds:bounds];
    iconRect.origin.x += 10; //像右边偏15
    return iconRect;
}
// UITextField 文字与输入框的距离
- (CGRect)textRectForBounds:(CGRect)bounds{
    return CGRectInset(bounds, 30, 0);
}

// 控制文本的位置
- (CGRect)editingRectForBounds:(CGRect)bounds{
    return CGRectInset(bounds, 30, 0);
}

@end
