//
//  XFProgressHUD.m
//  FengLei
//
//  Created by liaoxf on 2017/12/4.
//  Copyright © 2017年 com.mlj.FengLei. All rights reserved.
//

#import "XFProgressHUD.h"
#import "MBProgressHUD.h"
#import "UIImage+XFExtension.h"

@implementation XFProgressHUD

+ (MBProgressHUD *)showMessage:(NSString *)message {
    UIWindow *view = [UIApplication sharedApplication].delegate.window;
    return [self showMessage:message inView:view];
}

+ (MBProgressHUD *)showMessage:(NSString *)message inView:(UIView *)view {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    
    hud.mode = MBProgressHUDModeText;
    hud.label.text = message;
    
    hud.label.textColor = [UIColor whiteColor];
    //    hud.bezelView.blurEffectStyle = UIBlurEffectStyleDark;
    //    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    hud.bezelView.backgroundColor =  [UIColor colorWithRed:0 green:0 blue:0 alpha:1];
    hud.label.numberOfLines = 0;
    
    // Move to bottm center.
    //    hud.offset = CGPointMake(0.f, MBProgressMaxOffset);
    
    [hud hideAnimated:YES afterDelay:3.f];
    return hud;
}

+ (MBProgressHUD *)showLoading {
    UIWindow *view = [UIApplication sharedApplication].delegate.window;
    return [self showLoadingInView:view];
}

+ (MBProgressHUD *)showLoadingInView:(UIView *)view {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.bezelView.backgroundColor =  [UIColor colorWithRed:0 green:0 blue:0 alpha:1];
    //    [UIActivityIndicatorView appearanceWhenContainedInInstancesOfClasses:@[[MBProgressHUD class]]].color = [UIColor redColor];
    hud.contentColor = [UIColor whiteColor];
    return hud;
}

+ (MBProgressHUD *)showFengLeiLoading {
    UIWindow *view = [UIApplication sharedApplication].delegate.window;
    return [self showFengLeiLoadingInView:view];
}

+ (MBProgressHUD *)showFengLeiLoadingInView:(UIView *)view {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeCustomView;
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageWithName:@"loading" bundle:@"XFWidgets" targetClass:[XFProgressHUD class]]];
    
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat:M_PI];
    rotationAnimation.removedOnCompletion = NO;
    
    
    //每次旋转的时间（单位秒）
    rotationAnimation.duration = 1.5;
    
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = MAXFLOAT;
    [imageView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    
    hud.customView = imageView;
    hud.bezelView.backgroundColor =  [UIColor colorWithRed:0 green:0 blue:0 alpha:1];
    hud.margin = 15;
//    hud.label.text = @"加载中";
//    hud.label.textColor = [UIColor whiteColor];
    return hud;
}

+ (void)dismissHUD:(MBProgressHUD *)hud {
    [hud hideAnimated:YES];
}

@end
