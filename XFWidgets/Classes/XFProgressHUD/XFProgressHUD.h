//
//  XFProgressHUD.h
//  FengLei
//
//  Created by liaoxf on 2017/12/4.
//  Copyright © 2017年 com.mlj.FengLei. All rights reserved.
//

#import <Foundation/Foundation.h>
@class MBProgressHUD;

@interface XFProgressHUD : NSObject

+ (MBProgressHUD *)showMessage:(NSString *)message;
+ (MBProgressHUD *)showMessage:(NSString *)message inView:(UIView *)view;

+ (MBProgressHUD *)showLoading;
+ (MBProgressHUD *)showLoadingInView:(UIView *)view;

+ (MBProgressHUD *)showFengLeiLoading;
+ (MBProgressHUD *)showFengLeiLoadingInView:(UIView *)view;

+ (void)dismissHUD:(MBProgressHUD *)hud;


@end
