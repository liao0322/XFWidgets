//
//  XFTextCarouselView.h
//  TextCarouselDemo
//
//  Created by liaoxf on 2018/3/13.
//  Copyright © 2018年 liaoxf. All rights reserved.
//

#import <UIKit/UIKit.h>

@class XFTextCarouselView;

@protocol XFTextCarouselViewDelegate<NSObject>

- (void)textCarouselView:(XFTextCarouselView *)textCarouselView didSelectedIndex:(NSInteger)index;

@end

@interface XFTextCarouselView : UIView


/**
 文本数组
 */
@property (strong, nonatomic) NSArray<NSString *> *textArray;

/**
 动画间隔时间
 */
@property (assign, nonatomic) NSTimeInterval timeInterval;

/**
 文本颜色
 */
@property (strong, nonatomic) UIColor *textColor;

/**
 文本字体
 */
@property (strong, nonatomic) UIFont *textFont;

@property (weak, nonatomic) id<XFTextCarouselViewDelegate> delegate;

@end
