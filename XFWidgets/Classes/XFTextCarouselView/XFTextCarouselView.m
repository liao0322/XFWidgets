//
//  XFTextCarouselView.m
//  TextCarouselDemo
//
//  Created by liaoxf on 2018/3/13.
//  Copyright © 2018年 liaoxf. All rights reserved.
//

#import "XFTextCarouselView.h"

@interface XFTextCarouselView ()
<
    UITableViewDataSource,
    UITableViewDelegate
>

@property (nonatomic, strong) UITableView *tableView;
@property (strong, nonatomic) NSTimer *timer;

@end

@implementation XFTextCarouselView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubviews];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.tableView.frame = self.bounds;
}

- (void)addSubviews {
    [self addSubview:self.tableView];
    self.tableView.rowHeight = CGRectGetHeight(self.frame);
}

- (void)test {
    NSIndexPath *topIndexPath = [self.tableView.indexPathsForVisibleRows firstObject];
    NSInteger topRow = topIndexPath.row;
    topRow++;
    if (topRow == self.textArray.count) {
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionNone animated:NO];
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] atScrollPosition:UITableViewScrollPositionNone animated:YES];
    } else {
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:topRow inSection:0] atScrollPosition:UITableViewScrollPositionNone animated:YES];
    }
}

- (void)willMoveToSuperview:(UIView *)newSuperview {
    if (self.textArray.count > 1) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(self.timeInterval * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self addTimer];
        });
    }
}

- (void)setTextArray:(NSArray *)textArray {

    if (textArray.count > 1) {
        NSString *firstText = [textArray firstObject];
        NSMutableArray *tempArr = [NSMutableArray arrayWithArray:textArray];
        [tempArr addObject:firstText];
        _textArray = [tempArr copy];
        
    } else {
        _textArray = textArray;
    }
}

- (void)addTimer {
    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
    [self.timer fire];
}


- (void)stopTimer {
    [self.timer invalidate];
    self.timer = nil;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.textArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UILabel *tLabel = [[UILabel alloc] init];
    tLabel.text = self.textArray[indexPath.row];
    tLabel.frame = CGRectMake(15, 0, CGRectGetWidth(self.frame) - 30, CGRectGetHeight(self.frame));
    tLabel.textColor = self.textColor;
    tLabel.font = self.textFont;
    
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    [cell addSubview:tLabel];
    return cell;
    
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (indexPath.row == self.textArray.count - 1) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(textCarouselView:didSelectedIndex:)]) {
            [self.delegate textCarouselView:self didSelectedIndex:0];
        }
    } else {
        if (self.delegate && [self.delegate respondsToSelector:@selector(textCarouselView:didSelectedIndex:)]) {
            [self.delegate textCarouselView:self didSelectedIndex:indexPath.row];
        }
    }
}

#pragma mark - LazyLoad

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.dataSource = self;
        _tableView.pagingEnabled = YES;
        _tableView.scrollEnabled = NO;
        _tableView.delegate = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor clearColor];
    }
    return _tableView;
}


- (NSTimer *)timer {
    if (!_timer) {
        _timer = [NSTimer timerWithTimeInterval:self.timeInterval target:self selector:@selector(test) userInfo:nil repeats:YES];
    }
    return _timer;
}


@end
