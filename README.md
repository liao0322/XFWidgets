# XFWidgets

[![CI Status](http://img.shields.io/travis/liao0322/XFWidgets.svg?style=flat)](https://travis-ci.org/liao0322/XFWidgets)
[![Version](https://img.shields.io/cocoapods/v/XFWidgets.svg?style=flat)](http://cocoapods.org/pods/XFWidgets)
[![License](https://img.shields.io/cocoapods/l/XFWidgets.svg?style=flat)](http://cocoapods.org/pods/XFWidgets)
[![Platform](https://img.shields.io/cocoapods/p/XFWidgets.svg?style=flat)](http://cocoapods.org/pods/XFWidgets)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

XFWidgets is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'XFWidgets'
```

## Author

liao0322, liao0322@hotmail.com

## License

XFWidgets is available under the MIT license. See the LICENSE file for more info.
